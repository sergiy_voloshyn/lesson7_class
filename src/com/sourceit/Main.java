package com.sourceit;

import java.util.*;

public class Main {

    public static void main(String[] args) {
        // write your code here

/*
        List<String> list = new ArrayList<>();
        list.add("Test 1");
        list.add("Test 2");
        list.add("Test 3");
        list.add("Test 4");
        list.add("Test 5");
//        System.out.println(list);
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
*/

        LinkedList<User> list = new LinkedList<>();
        list.add(new User("Vasya", 32));
        list.add(new User("Petya", 42));
        list.add(new User("Sasha", 52));
        list.add(new User("Alyosha", 72));

        Collections.sort(list);

        for (User user : list) {

            System.out.println(user);
        }


        Collections.sort(list, new Sort());

        System.out.println();

        for (User user : list) {
            System.out.println(user);
        }


    }


    static class Sort implements Comparator<User> {
        @Override
        public int compare(User user1, User user2) {
            if (user1.age - user2.age != 0) {
                return user1.age - user2.age;
            } else {
                return user1.name.compareTo(user2.name);
            }
        }

    }


    static class User implements Comparable<User> {
        String name;
        int age;

        public User(String name, int age) {
            this.name = name;
            this.age = age;
        }

        @Override
        public int compareTo(User user) {

            return name.compareTo(user.name);

            /*if (name.compareTo(user.name) != 0) {
                return name.compareTo(user.name);
            } else {
                return age - user.age;
            }*/
        }


        @Override
        public String toString() {
            return "User{" +
                    "name='" + name + '\'' +
                    ", age=" + age +
                    '}';
        }
    }

}
// list2.add(" e");


         /*   Iterator<String> iterator = list2.iterator();
            while (iterator.hasNext()) {
                //  System.out.println(iterator.next());

                String value = iterator.next();
                if (value.equals(" c")) {

                    iterator.remove();
                } else {
                    System.out.println(value);
                }
            }


        for (int i = 0; i < list2.size(); i++) {
            System.out.println( list2.get(i));

        }

*/





//  list2.forEach();
/*

Интерфейс Comparable содержит один единственный метод int compareTo(E item), который сравнивает текущий объект с
объектом, переданным в качестве параметра. Если этот метод возвращает отрицательное число, то текущий объект будет
располагаться перед тем, который передается через параметр. Если метод вернет положительное число, то, наоборот,
 после второго объекта. Если метод возвратит ноль, значит, оба объекта равны.

 int compare(T a, T b);

Метод compare также возвращает числовое значение - если оно отрицательное, то объект a предшествует объекту b,
иначе - наоборот. А если метод возвращает ноль, то объекты равны. Для применения интерфейса нам вначале надо
создать класс компаратора, который реализует этот интерфейс:
*/

